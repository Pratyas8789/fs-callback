const fs = require('fs');
const path = require('path');


function directoryCreated(folderPath, callBackFunc) {
    fs.access(folderPath, error => {
        if (error) {
            console.log('first time folder created !!!!!!!!!!');
            fs.mkdir(folderPath, error => {
                if (error) {
                    throw (error);
                } else {
                    callBackFunc()
                }
            })
        } else {
            console.log('already folder exists !!!!!!!!!!');
            callBackFunc()
        }
    })
}

function firstFileCreatedAfterDeleted(folderPath, num) {
    for (let i = 0; i < num; i++) {
        let fileName = `file${i}` + '.json'
        fs.writeFile(path.join(folderPath, fileName), 'w', (error) => {
            if (error) {
                throw (error);
            } else {
                console.log(fileName + " " + "created");
                fs.unlink(path.join(folderPath, fileName), (error) => {
                    if (error) {
                        throw (error);
                    } else {
                        console.log(fileName + " " + "deleted");
                    }
                })
            }
        })
    }
}

module.exports.directoryCreated = directoryCreated;
module.exports.firstFileCreatedAfterDeleted = firstFileCreatedAfterDeleted;