const fs = require('fs');

function readingFunction(fileData, callBack) {
    fs.readFile(fileData, 'utf8', (error, data) => {
        if (error) {
            throw (error);
        }
        else {
            callBack(data);
        }
    });
}

function convertUpperCase(data, callBack) {
    data = data.toUpperCase();
    fs.writeFile("upperCase.txt", data, error => {
        if (error) {
            throw (error);
        } else {
            fs.writeFile('filenames.txt', "upperCase.txt", (error) => {
                if (error) {
                    throw (error);
                } else {
                    console.log("upperCase.txt created successfully ");
                    callBack(data);
                }
            })
        }
    });
}


function append(pathOfFile) {
    fs.appendFile("filenames.txt", "\n" + pathOfFile, error => {
        if (error) {
            throw (error);
        }
        else {
            console.log(`${pathOfFile} created successfully`);
        }
    })
}

function convertLowerCase(data, callBack) {
    data = data.toLowerCase().split('. ').join('.\n')
    fs.writeFile("lowerCase.txt", data, error => {
        if (error) {
            throw (error);
        } else {
            append("lowerCase.txt");
            callBack(data);
        }
    })
}

function sort(data, callBack) {

    fs.readFile("upperCase.txt", 'utf8', (error, upperFileData) => {
        if (error) {
            throw (error);
        }
        else {
            let allData = upperFileData + data;
            allData = allData.split(" ").sort().toString();
            fs.writeFile("sortFile.txt", allData, error => {
                if (error) {
                    throw (error);
                } else {
                    append("sortFile.txt");
                    callBack();
                }
            })
        }
    });

}

function deleteFile() {
    fs.readFile('filenames.txt', 'utf8', (error, data) => {
        if (error) {
            throw (error);
        }
        else {
            let dataarray = data.split('\n');
            dataarray.map((ele) => {
                fs.unlink(ele, (error) => {
                    if (error) {
                        throw (error);
                    } else {
                        console.log(ele + " " + "deleted sucessfully");
                    }
                })
            })
        }
    });
}


module.exports.readingFunction = readingFunction;
module.exports.convertUpperCase = convertUpperCase
module.exports.convertLowerCase = convertLowerCase;
module.exports.sort = sort;
module.exports.deleteFile = deleteFile