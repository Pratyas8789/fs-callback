const problem1 = require('../problem1');

const folderPath = './files';

try {
    problem1.directoryCreated(folderPath, () => {
        problem1.firstFileCreatedAfterDeleted(folderPath, 5);
    })
} catch (error) {
    console.error(error.message);
}