const problem2 = require('../problem2');
const path = require('path');

const fileData = "lipsum.txt"

try {
    problem2.readingFunction(fileData, (data1) => {
        problem2.convertUpperCase(data1, (data2) => {
            problem2.convertLowerCase(data2, (data3) => {
                problem2.sort(data3, () => {
                    problem2.deleteFile()
                })
            })
        })
    });
} catch (error) {
    console.error(error.message);
}
